<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Logincontroller;
use App\Http\Controllers\RegisterController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/unit/{unit}', 'UnitController@show')->name('unit.show');



$unit_post = [
    [
        "title" => "Judul Pertanian",
        "slug" => "unit-pertanian",
        "sub_title" => "Produk Komoditas",
        "body_1" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta enim alias consequuntur impedit, iste, accusamus ab assumenda delectus quibusdam quidem natus praesentium quam dolores ea vero repellat architecto nihil aut nulla ullam doloribus! Officia laborum obcaecati ducimus fugit ratione quasi quis doloremque dolores neque nihil quos odio, nulla totam similique ipsum, fuga veniam eum tempore eveniet itaque unde mollitia. Tempore optio facilis nostrum dignissimos illo et dolores iste, expedita tempora aut eum maxime veniam voluptatibus dolorum quia consequuntur recusandae in?",
        "paragraf" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta enim alias consequuntur impedit, iste, accusamus ab assumenda delectusLorem ipsum dolor sit amet consectetur adipisicing elit. Soluta enim alias consequuntur impedit, iste, accusamus ab assumenda delectusLorem ipsum dolor sit amet consectetur adipisicing elit. Soluta enim alias consequuntur impedit, iste, accusamus ab assumenda delectus"
    ],
    [
        "title" => "Judul Peternakan",
        "slug" => "unit-peternakan",
        "sub_title" => "Produk Ternak",
        "body_1" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta enim alias consequuntur impedit, iste, accusamus ab assumenda delectus quibusdam quidem natus praesentium quam dolores ea vero repellat architecto nihil aut nulla ullam doloribus! Officia laborum obcaecati ducimus fugit ratione quasi quis doloremque dolores neque nihil quos odio, nulla totam similique ipsum, fuga veniam eum tempore eveniet itaque unde mollitia. Tempore optio facilis nostrum dignissimos illo et dolores iste, expedita tempora aut eum maxime veniam voluptatibus dolorum quia consequuntur recusandae in?",
        "paragraf" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta enim alias consequuntur impedit, iste, accusamus ab assumenda delectusLorem ipsum dolor sit amet consectetur adipisicing elit. Soluta enim alias consequuntur impedit, iste, accusamus ab assumenda delectusLorem ipsum dolor sit amet consectetur adipisicing elit. Soluta enim alias consequuntur impedit, iste, accusamus ab assumenda delectus"
    ],
    [
        "title" => "Judul Koperasi",
        "slug" => "unit-koperasi",
        "sub_title" => "Penjualan Produk Hasil Produksi",
        "body_1" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta enim alias consequuntur impedit, iste, accusamus ab assumenda delectus quibusdam quidem natus praesentium quam dolores ea vero repellat architecto nihil aut nulla ullam doloribus! Officia laborum obcaecati ducimus fugit ratione quasi quis doloremque dolores neque nihil quos odio, nulla totam similique ipsum, fuga veniam eum tempore eveniet itaque unde mollitia. Tempore optio facilis nostrum dignissimos illo et dolores iste, expedita tempora aut eum maxime veniam voluptatibus dolorum quia consequuntur recusandae in?",
        "paragraf" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta enim alias consequuntur impedit, iste, accusamus ab assumenda delectusLorem ipsum dolor sit amet consectetur adipisicing elit. Soluta enim alias consequuntur impedit, iste, accusamus ab assumenda delectusLorem ipsum dolor sit amet consectetur adipisicing elit. Soluta enim alias consequuntur impedit, iste, accusamus ab assumenda delectus"
    ],
    [
        "title" => "Judul Pelatihan",
        "slug" => "unit-pelatihan",
        "sub_title" => "Program Pelatihan Petani Cerdas",
        "body_1" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta enim alias consequuntur impedit, iste, accusamus ab assumenda delectus quibusdam quidem natus praesentium quam dolores ea vero repellat architecto nihil aut nulla ullam doloribus! Officia laborum obcaecati ducimus fugit ratione quasi quis doloremque dolores neque nihil quos odio, nulla totam similique ipsum, fuga veniam eum tempore eveniet itaque unde mollitia. Tempore optio facilis nostrum dignissimos illo et dolores iste, expedita tempora aut eum maxime veniam voluptatibus dolorum quia consequuntur recusandae in?",
        "paragraf" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta enim alias consequuntur impedit, iste, accusamus ab assumenda delectusLorem ipsum dolor sit amet consectetur adipisicing elit. Soluta enim alias consequuntur impedit, iste, accusamus ab assumenda delectusLorem ipsum dolor sit amet consectetur adipisicing elit. Soluta enim alias consequuntur impedit, iste, accusamus ab assumenda delectus"
    ],
];




Route::get('/', function () {
    $isi_paragraf = [
        "isi1" => "Okiagaru Farm adalah sebuah inisiatif yang lahir dari hasrat kami untuk memajukan pertanian organik di Indonesia. Sejak tahun 2004, kami telah berkomitmen untuk mengembangkan metode pertanian yang ramah lingkungan, dengan fokus pada budidaya padi organik, produksi kompos alami, dan penggunaan pestisida nabati. Di lahan seluas 1,8 hektar, kami telah melalui perjalanan yang panjang dan penuh dedikasi untuk menciptakan lingkungan pertanian yang berkelanjutan dan subur. Selama perjalanan kami, kami telah menemukan banyak tantangan dan peluang dalam dunia pertanian organik. Kami berupaya untuk terus belajar dan berinovasi, mencari solusi terbaik untuk meningkatkan produktivitas pertanian tanpa merusak lingkungan. Kami percaya bahwa dengan berkolaborasi dengan alam, kami dapat menciptakan lingkungan yang harmonis dan menghasilkan produk pertanian berkualitas tinggi.",
        "isi2" => "Berlokasi di kampung Tunggilis, Desa Ciputri, Kecamatan Pacet, Kabupaten Cianjur, Jawa Barat, Okiagaru Farm berada di lingkungan geografis yang sangat mendukung. Terletak di kaki Gunung Gede, wilayah ini memiliki sebagian besar dataran tinggi pegunungan yang memberikan iklim yang ideal untuk pertanian organik. Namun, sebagian lainnya adalah area perkebunan yang subur, menciptakan kesempatan yang unik untuk menghasilkan produk pertanian berkualitas tinggi. Kami terus berkomitmen untuk menjadi bagian dari pergerakan pertanian organik di Indonesia, berusaha memberikan solusi pertanian yang berkelanjutan dan berkualitas. Dengan semangat dan upaya yang tak henti-hentinya, kami siap berkontribusi positif dalam pemenuhan kebutuhan pangan yang lebih sehat dan berkelanjutan untuk masyarakat Indonesia.",

    ];
    return view('index', compact('isi_paragraf'));
});

Route::get('/Blog', function () {
    return view('blog');
});

Route::get('/Tentang', function () {
    return view('tentang');
});

Route::get('/Login', [Logincontroller::class, 'index']);
Route::post('/Login', [Logincontroller::class, 'authenticate']);

Route::get('/Register', [RegisterController::class, 'regis']);
Route::post('/Register', [RegisterController::class, 'store']);

Route::get('/Produk', function () {
    return view('produk');
});


Route::get('/admin', function () {
    return view('dashboard/admin');
});



Route::get('/Pertanian', function () {
    $unit_post = [
    [
        "title" => "UNIT PERTANIAN",
        "slug" => "unit-pertanian",
        "sub_title" => "Produk Komoditas11",
        "body_1" => "Unit Pertanian adalah salah satu bagian penting dari perusahaan kami yang berfokus pada budidaya tanaman organik. Kami dengan bangga menawarkan berbagai jenis produk komoditas berkualitas tinggi yang berasal dari pertanian kami sendiri. Setiap tanaman kami dikelola dengan hati-hati, tanpa menggunakan pestisida atau bahan kimia berbahaya. Kami berkomitmen untuk memastikan bahwa setiap produk yang kami hasilkan adalah yang terbaik dalam kualitas dan keamanan. Dengan menggabungkan teknologi modern dan metode tradisional, kami berusaha untuk menjadi pemimpin dalam industri pertanian organik.",
        "paragraf" => "Kami di Unit Pertanian juga peduli akan pendidikan dan pelatihan dalam pertanian organik. Kami menawarkan berbagai program pelatihan dan magang untuk individu yang tertarik untuk belajar lebih lanjut tentang pertanian organik. Program ini mencakup panduan praktis, demonstrasi lapangan, dan akses ke para ahli pertanian yang berpengalaman. Kami percaya bahwa dengan berbagi pengetahuan dan pengalaman, kami dapat membantu mendukung komunitas pertanian organik yang berkembang dan berkelanjutan. Unit Pertanian kami adalah tempat yang ideal untuk menjalani perjalanan Anda dalam dunia pertanian yang sehat dan berkelanjutan."
    ],
];
    return view('unit/pertanian', ['unit_post' => $unit_post]);

});







Route::get('/Peternakan', function () {
    $unit_post = [
    [
        "title" => "UNIT PETERNAKAN",
        "slug" => "unit-peternakan",
        "sub_title" => "Produk Ternak",
        "body_1" => "Unit Peternakan kami berkomitmen untuk menyediakan produk hewani berkualitas tinggi yang berasal dari lingkungan yang sehat dan terjaga. Hewan-hewan kami dipelihara dengan cermat, diberi makanan berkualitas tinggi, dan diberikan ruang gerak yang memadai. Kami mengutamakan kesejahteraan hewan, karena kami percaya bahwa hewan yang bahagia dan sehat menghasilkan produk yang lebih baik. Dengan standar kebersihan dan keamanan yang ketat, Anda dapat yakin bahwa produk peternakan kami adalah pilihan terbaik untuk kesehatan Anda dan keluarga Anda.",
        "paragraf" => "Di Unit Peternakan, kami juga memahami pentingnya edukasi peternakan yang bertanggung jawab. Kami menawarkan berbagai program pelatihan dan workshop untuk individu yang tertarik dalam dunia peternakan. Dari manajemen ternak hingga praktik pemeliharaan yang tepat, kami berbagi pengetahuan kami dengan harapan dapat membantu meningkatkan keterampilan dan pengetahuan dalam industri peternakan. Dengan bergabung bersama kami, Anda tidak hanya mendukung produk peternakan berkualitas tinggi, tetapi juga ikut berkontribusi dalam memajukan praktik peternakan yang berkelanjutan. Selamat datang di Unit Peternakan kami, di mana kami tumbuh bersama dalam menyediakan produk peternakan yang terbaik."
    ],
];
    return view('unit/peternakan', ['unit_post' => $unit_post]);
});





Route::get('/Koperasi', function () {
    $unit_post = [
    [
        "title" => "UNIT KOPERASI",
        "slug" => "unit-koperasi",
        "sub_title" => "Penjualan Produk Hasil Produksi",
        "body_1" => "Unit Koperasi kami adalah fondasi komunitas yang kuat di mana anggota kami bersatu untuk mencapai keberhasilan bersama. Kami berkomitmen untuk meningkatkan kualitas hidup anggota kami dengan menyediakan berbagai layanan keuangan dan dukungan. Dari pinjaman usaha kecil hingga rencana tabungan masa depan, kami berusaha untuk membantu mewujudkan impian finansial anggota kami. Koperasi kami adalah tempat di mana solidaritas dan kerjasama menghasilkan manfaat nyata bagi setiap individu.",
        "paragraf" => "Di Unit Koperasi, kami juga memahami pentingnya pendidikan keuangan dan pengembangan keterampilan. Oleh karena itu, kami menyelenggarakan berbagai program pelatihan dan seminar yang bertujuan untuk meningkatkan pemahaman anggota kami tentang manajemen keuangan dan investasi yang cerdas. Dengan berpartisipasi dalam unit koperasi kami, Anda tidak hanya menjadi bagian dari komunitas yang peduli, tetapi Anda juga memiliki akses ke sumber daya dan dukungan yang dapat membantu mencapai stabilitas finansial dan kesuksesan. Selamat datang di Unit Koperasi kami, di mana kami bersama-sama mewujudkan impian finansial Anda."
    ],
];
    return view('unit/koperasi', ['unit_post' => $unit_post]);
});

Route::get('/Pelatihan', function () {
    $unit_post = [
    [
        "title" => "UNIT PELATIHAN",
        "slug" => "unit-pelatihan",
        "sub_title" => "Program Pelatihan Petani Cerdas",
        "body_1" => "Unit Pelatihan kami berkomitmen untuk memberikan pelatihan berkualitas tinggi kepada individu yang ingin mengembangkan keterampilan dan pengetahuan mereka. Kami menawarkan berbagai program pelatihan yang dirancang untuk memenuhi berbagai kebutuhan, mulai dari kursus keterampilan teknis hingga pelatihan kepemimpinan. Dengan tim instruktur yang berpengalaman dan fasilitas modern, kami menciptakan lingkungan yang mendukung pertumbuhan dan pengembangan individu. Tujuan kami adalah membantu peserta pelatihan kami mencapai potensi penuh mereka dalam karir dan kehidupan pribadi mereka.",
        "paragraf" => "Di Unit Pelatihan kami, kami percaya bahwa pendidikan adalah kunci kesuksesan. Oleh karena itu, kami aktif dalam menyelenggarakan seminar dan lokakarya yang mencakup berbagai topik yang relevan dengan pasar kerja saat ini. Kami berusaha untuk mempersiapkan individu untuk tantangan masa depan dan memberikan mereka keunggulan kompetitif. Bergabunglah dengan kami di Unit Pelatihan untuk mendapatkan akses ke sumber daya pendidikan yang berkualitas tinggi dan memulai perjalanan Anda menuju kesuksesan. Selamat datang di tempat kami, di mana pembelajaran adalah kunci utama menuju masa depan yang cerah."
    ],
];
    return view('unit/pelatihan', ['unit_post' => $unit_post]);
});