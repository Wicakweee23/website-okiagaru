<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Support\Facades\Session; // Import Session
use App\Http\Requests\RegistrationRequest;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function regis()
    {
        return view('login.reg', [
            'title' => 'Register'
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|min:8|max:255',
            'username' => 'required|min:5|max:25|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8|max:255',
        ]);

        // Buat instance User model dengan data dari formulir
        $user = new User();
        $user->name = $request->input('nama');
        $user->username = $request->input('username');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password')); // Enkripsi password

        // Simpan pengguna ke database
        $user->save();

        

        // Redirect ke halaman yang sesuai setelah berhasil mendaftar
        return redirect('/Login'); // Sesuaikan dengan halaman login Anda
    }
}
