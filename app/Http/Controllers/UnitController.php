<?php

use App\Http\Controllers\Controller;

use App\Models\Unit;

class UnitController extends Controller
{
    public function show()
    {
        $units = Unit::all(); // Mengambil semua data dari tabel "units"
        return view('unit.index', compact('units'));

    }
}
