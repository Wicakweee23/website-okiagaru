<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama' => 'required|max:255',
            'username' => 'required|min:6|max:12|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6|max:255',
        ];
    }

    /**
     * Get custom error messages for validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'nama.required' => 'Kolom Nama wajib diisi. Minimal 8 Karakter',
            'username.required' => 'Kolom Username wajib diisi.',
            'username.min' => 'Username minimal harus 6 karakter.',
            'username.max' => 'Username maksimal harus 25 karakter.',
            'username.unique' => 'Username sudah digunakan.',
            'email.required' => 'Kolom Email wajib diisi.',
            'email.email' => 'Format email tidak valid.',
            'email.unique' => 'Email sudah digunakan.',
            'password.required' => 'Kolom Password wajib diisi.',
            'password.min' => 'Password minimal harus 8 karakter.',
            'password.max' => 'Password maksimal harus 255 karakter.',
        ];
    }
}
