<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Okiagaru | Unit</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">
  <link href="assets/fontawesome/css/all.css" rel="stylesheet">
  <link rel="stylesheet" href="assets/preloader/style.css">
  <script src="assets/preloader/script.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
  <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
  <link rel="stylesheet" href="assets/card/stylecard.css">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Tempo
  * Updated: Jul 27 2023 with Bootstrap v5.3.1
  * Template URL: https://bootstrapmade.com/tempo-free-onepage-bootstrap-theme/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

<script src="https://unpkg.com/aos@next/dist/aos.js"></script>
  <script>
    AOS.init();
  </script>
  <div id="preloader">
    <div id="loader"></div>
  </div>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top header-inner-pages">
    <div class="container d-flex align-items-center justify-content-between">

      <h1 class="logo"><a href="/">Okiagaru</a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="index.html" class="logo"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

      <nav id="navbar" class="navbar">
        <ul>
          <li><a class="nav-link scrollto" href="/">Beranda</a></li>
          <li><a class="nav-link scrollto" href="/">Tentang</a></li>
          <li><a class="nav-link scrollto active" href="/">Unit</a></li>
          <li><a class="nav-link scrollto" href="/">Produk</a></li>
          <li><a class="nav-link scrollto" href="/">Tim</a></li>
          <!-- Navbar Cabang -->
          <li class="dropdown"><a href="#"><span>Program</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
              <li><a href="#">Magang ke Jepang</a></li>
              <!-- Buka Comment untuk menampilkan cabang cabang navigasi -->
              <!-- <li class="dropdown"><a href="#"><span>Program Cabang</span> <i class="bi bi-chevron-right"></i></a>
                <ul>
                  <li><a href="#">Cabang 1</a></li>
                  <li><a href="#">Cabang 2</a></li>
                  <li><a href="#">Cabang 3</a></li>
                  <li><a href="#">Cabang 4</a></li>
                  <li><a href="#">Cabang 5</a></li>
                </ul>
              </li> -->
              <li><a href="#">Pengolahan Hasil Pertanian</a></li>
              <li><a href="#">Pusat Pelatihan Pertanian</a></li>
              <li><a href="#">Budidaya Sayuran Organik</a></li>
            </ul>
          </li>
          <li><a class="nav-link scrollto" href="#contact">Kontak</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->

  <main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <ol>
          <li><a href="/">Beranda</a></li>
          <li>Unit</li>
        </ol>
        <h2>Unit Perusahaan</h2>

      </div>
    </section><!-- End Breadcrumbs -->
   
    
    <!-- ======= Cta Section ======= -->
    <section id="cta" class="cta3">
      <div class="container">

        <div class="text-center">
          <h3><strong>{{ $unit_post[0]['title'] }}</strong></h3>
        </div>

      </div>
    </section><!-- End Cta Section -->

<br>


        
<!-- ======= About Section ======= -->
    <section class="about">
      <div class="container">

        <div class="section-title">
          <h2 class="animate__animated animate__fadeInUp animate__delay-1s">Unit</h2>
          <h3 class="animate__animated animate__fadeInUp animate__delay-1s">Penjualan Hasil <span>Produksi</span></h3>
          <!-- buka comment dibawah ini jika ingin menambahkan sub judul berupa 1 - 3 kalimat -->
          <!-- <p>Berfokus kepada budidaya tanaman padi, pembuatan kompos, dan pestisida nabati. di lahan seluas 1,8 hektar sejak tahun 2004.</p> -->
        </div>

        <div class="row content" >
          <div class="col-lg-6">
            <p class="animate__animated animate__fadeInUp animate__delay-2s">
            {{ $unit_post[0]['body_1'] }}
            </p>

          </div>
          <div class="col-lg-6 pt-4 pt-lg-0" >
            <p class="animate__animated animate__fadeInUp animate__delay-2s">
            {{ $unit_post[0]['paragraf'] }}
            </p>

          </div>
        </div>

      </div>
    </section><!-- End About Section -->


        <!-- Card Section -->
        <div class="card-category-2">
          <div class="img-card iCard-style1" data-aos="fade-down-right">
              <div class="card-content">
                  <div class="card-image">
                        <span class="card-title"></span>
                        <img src="assets/img/1.png"/>
                  </div>
                            
              <div class="card-text">
                                <p>
                                Agus Ali Nurdin, SE adalah sosok millenial yang sukses menggeluti usaha pertanian.
                                </p>
                            </div>
                            
                        </div>
                        
                        <div class="card-link">
                            <a href="https://www.youtube.com/watch?v=r-rjsYZGKnE" title="Read Full"><span>Baca Selengkapnya</span></a>
                        </div>
                    </div> 


                    <div class="img-card iCard-style1" data-aos="zoom-out-up">
              <div class="card-content">
                  <div class="card-image">
                        <span class="card-title"></span>
                        <img src="assets/img/2.png"/>
                  </div>
                            
              <div class="card-text">
                                <p>
                                Pertanian menjadi salah satu sektor yang berperan besar dalam ketahanan pangan di Indonesia. 
                                </p>
                            </div>
                            
                        </div>
                        
                        <div class="card-link">
                            <a href="https://www.youtube.com/watch?v=yDOup72h8-s" title="Read Full"><span>Baca Selengkapnya</span></a>
                        </div>
                    </div> 



                    <div class="img-card iCard-style1" data-aos="fade-down-left">
              <div class="card-content">
                  <div class="card-image">
                        <span class="card-title"></span>
                        <img src="assets/img/3.png"/>
                  </div>
                            
              <div class="card-text">
                                <p>
                                    Petani cerdas yang disiplin dan memiliki akhlak mulia adalah kunci dari lahan kami.
                                </p>
                            </div>
                            
                        </div>
                        
                        <div class="card-link">
                            <a href="https://www.youtube.com/watch?v=v2YRXI6pZ7I" title="Read Full"><span>Baca Selengkapnya</span></a>
                        </div>
                    </div> 

        </div>





<!-- ======= Footer ======= -->
<footer id="footer">

<div class="footer-top">
  <div class="container">
    <div class="row">

      <div class="col-lg-3 col-md-6 footer-contact">
        <h3>Okiagaru</h3>
        <p>
          Kp Tunggilis, Ciputri, Kec. Pacet, <br>
          Kabupaten Cianjur, Jawa Barat 43253<br>
          Indonesia <br><br>
          <strong>Phone:</strong> +62 856 9128 1247<br>
          <strong>Email:</strong> contoh@email.com<br>
        </p>
      </div>

      <div class="col-lg-2 col-md-6 footer-links">
        <h4>Menu</h4>
        <ul>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Beranda</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Tentang</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Unit</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Produk</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Tim</a></li>
        </ul>
      </div>

      <div class="col-lg-3 col-md-6 footer-links">
        <h4>Unit kami</h4>
        <ul>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Pertanian</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Peternakan</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Koperasi</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Pelatihan</a></li>
        </ul>
      </div>

      <div class="col-lg-4 col-md-6 footer-newsletter">
        <h4>Mari bergabung</h4>
        <p>isi email untuk berlangganan</p>
        <form action="" method="post">
          <input type="email" name="email" placeholder="contoh@email.com"><input type="submit" value="Subscribe">
        </form>
      </div>

    </div>
  </div>
</div>

<div class="container d-md-flex py-4">

  <div class="me-md-auto text-center text-md-start">
    <div class="copyright">
      &copy; Copyright <strong><span>Okiagaru</span></strong>. 2023
    </div>
    <div class="credits">
      <!-- All the links in the footer should remain intact. -->
      <!-- You can delete the links only if you purchased the pro version. -->
      <!-- Licensing information: https://bootstrapmade.com/license/ -->
      <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/tempo-free-onepage-bootstrap-theme/ -->

    </div>
  </div>
  <div class="social-links text-center text-md-right pt-3 pt-md-0">
    <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
    <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
    <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
    <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
    <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
  </div>
</div>
</footer><!-- End Footer -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>