<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Okiagaru | Produk</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">
  <link href="assets/fontawesome/css/all.css" rel="stylesheet">
  <link rel="stylesheet" href="assets/preloader/style.css">
  <script src="assets/preloader/script.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
  <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Tempo
  * Updated: Jul 27 2023 with Bootstrap v5.3.1
  * Template URL: https://bootstrapmade.com/tempo-free-onepage-bootstrap-theme/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

<script src="https://unpkg.com/aos@next/dist/aos.js"></script>
  <script>
    AOS.init();
  </script>

<div id="preloader">
    <div id="loader"></div>
  </div>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top header-inner-pages">
    <div class="container d-flex align-items-center justify-content-between">

      <a href="/" class="logo"><img src="assets/img/logo.png" alt="logo okiagaru" class="img-fluid"></a>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="index.html" class="logo"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

      <nav id="navbar" class="navbar">
        <ul>
          <li><a class="nav-link scrollto" href="/">Beranda</a></li>
          <li><a class="nav-link scrollto" href="/">Tentang</a></li>
          <li><a class="nav-link scrollto" href="/">Unit</a></li>
          <li><a class="nav-link scrollto active" href="/">Produk</a></li>
          <li><a class="nav-link scrollto" href="/">Tim</a></li>
          <!-- Navbar Cabang -->
          <li class="dropdown"><a href="#"><span>Program</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
              <li><a href="#">Magang ke Jepang</a></li>
              <!-- Buka Comment untuk menampilkan cabang cabang navigasi -->
              <!-- <li class="dropdown"><a href="#"><span>Program Cabang</span> <i class="bi bi-chevron-right"></i></a>
                <ul>
                  <li><a href="#">Cabang 1</a></li>
                  <li><a href="#">Cabang 2</a></li>
                  <li><a href="#">Cabang 3</a></li>
                  <li><a href="#">Cabang 4</a></li>
                  <li><a href="#">Cabang 5</a></li>
                </ul>
              </li> -->
              <li><a href="#">Pengolahan Hasil Pertanian</a></li>
              <li><a href="#">Pusat Pelatihan Pertanian</a></li>
              <li><a href="#">Budidaya Sayuran Organik</a></li>
            </ul>
          </li>
          <li><a class="nav-link scrollto" href="#contact">Kontak</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->

  <main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <ol>
          <li><a href="/">Beranda</a></li>
          <li>Produk</li>
        </ol>
        <h2>Produk Hasil Panen</h2>

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Portfolio Details Section ======= -->
    <section id="portfolio-details" class="portfolio-details">
      <div class="container">

        <div class="row gy-4">

          <div class="col-lg-8">
            <div class="portfolio-details-slider swiper">
              <div class="swiper-wrapper align-items-center">

                <div class="swiper-slide">
                  <img src="assets/img/portfolio/portfolio-details-1.png" alt="">
                </div>

                <div class="swiper-slide">
                  <img src="assets/img/portfolio/portfolio-details-2.png" alt="">
                </div>

                <div class="swiper-slide">
                  <img src="assets/img/portfolio/portfolio-details-3.png" alt="">
                </div>

              </div>
              <div class="swiper-pagination"></div>
            </div>
          </div>

          <div class="col-lg-4">
            <div class="portfolio-info">
              <h3>Informasi Produk</h3>
              <ul>
                <li><strong>Kategori</strong>: Produk Hasil Panen</li>
                <li><strong>Tipe</strong>: Buah dan Sayur</li>
                <li><strong>Kadaluarsa</strong>: 2 Minggu (suhu ruangan)</li>
                <li><strong>Link Pembelian</strong>: <a href="#">www.tokopedia.com</a></li>
              </ul>
            </div>
            <div class="portfolio-description">
              <h2>Deskripsi Produk</h2>
              <p>
                Produk hasil panen di pertanian Okiagaru menghasilkan produk sayuran dan buah - buahan berkualitas. Mengandung serat dan vitamin untuk memenuhi kebutuhan sehari - hari. Produk dipilih dengan baik agar menghasilkan kualitas prima dan organik.
              </p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Portfolio Details Section -->

  </main><!-- End #main -->

<!-- ======= Footer ======= -->
<footer id="footer">

<div class="footer-top">
  <div class="container">
    <div class="row">

      <div class="col-lg-3 col-md-6 footer-contact">
        <h3>Okiagaru</h3>
        <p>
          Kp Tunggilis, Ciputri, Kec. Pacet, <br>
          Kabupaten Cianjur, Jawa Barat 43253<br>
          Indonesia <br><br>
          <strong>Phone:</strong> +62 856 9128 1247<br>
          <strong>Email:</strong> contoh@email.com<br>
        </p>
      </div>

      <div class="col-lg-2 col-md-6 footer-links">
        <h4>Menu</h4>
        <ul>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Beranda</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Tentang</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Unit</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Produk</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Tim</a></li>
        </ul>
      </div>

      <div class="col-lg-3 col-md-6 footer-links">
        <h4>Unit kami</h4>
        <ul>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Pertanian</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Peternakan</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Koperasi</a></li>
          <li><i class="bx bx-chevron-right"></i> <a href="#">Pelatihan</a></li>
        </ul>
      </div>

      <div class="col-lg-4 col-md-6 footer-newsletter">
        <h4>Mari bergabung</h4>
        <p>isi email untuk berlangganan</p>
        <form action="" method="post">
          <input type="email" name="email" placeholder="contoh@email.com"><input type="submit" value="Subscribe">
        </form>
      </div>

    </div>
  </div>
</div>

<div class="container d-md-flex py-4">

  <div class="me-md-auto text-center text-md-start">
    <div class="copyright">
      &copy; Copyright <strong><span>Okiagaru</span></strong>. 2023
    </div>
    <div class="credits">
      <!-- All the links in the footer should remain intact. -->
      <!-- You can delete the links only if you purchased the pro version. -->
      <!-- Licensing information: https://bootstrapmade.com/license/ -->
      <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/tempo-free-onepage-bootstrap-theme/ -->

    </div>
  </div>
  <div class="social-links text-center text-md-right pt-3 pt-md-0">
    <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
    <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
    <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
    <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
    <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
  </div>
</div>
</footer><!-- End Footer -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>