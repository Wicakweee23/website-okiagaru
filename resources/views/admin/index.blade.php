<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Okiagaru | Beranda</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">
  <link href="assets/fontawesome/css/all.css" rel="stylesheet">
  <link rel="stylesheet" href="assets/preloader/style.css">
  <script src="assets/preloader/script.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
  <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Tempo
  * Updated: Jul 27 2023 with Bootstrap v5.3.1
  * Template URL: https://bootstrapmade.com/tempo-free-onepage-bootstrap-theme/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>
<script src="https://unpkg.com/aos@next/dist/aos.js"></script>
  <script>
    AOS.init();
  </script>
  <div id="preloader">
    <div id="loader"></div>
  </div>


  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top ">
    <div class="container d-flex align-items-center justify-content-between">

      <!-- <h1 class="logo"><a href="/">Okiagaru</a></h1> -->
      <!-- Uncomment below if you prefer to use an image logo -->
      <a href="/" class="logo"><img src="assets/img/logo.png" alt="logo okiagaru" class="img-fluid"></a>

      <nav id="navbar" class="navbar">
        <ul>
          <li><a class="nav-link scrollto active" href="#hero">Beranda</a></li>
          <li><a class="nav-link scrollto" href="#about">Tentang</a></li>
          <li><a class="nav-link scrollto" href="#services">Unit</a></li>
          <li><a class="nav-link scrollto " href="#portfolio">Produk</a></li>
          <li><a class="nav-link scrollto" href="#team">Tim</a></li>

          <!-- Navbar Cabang -->
          <!-- <li class="dropdown"><a href="#"><span>Program</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
              <li><a href="#">Magang ke Jepang</a></li>
              <!-- Buka Comment untuk menampilkan cabang cabang navigasi -->
              <!-- <li class="dropdown"><a href="#"><span>Program Cabang</span> <i class="bi bi-chevron-right"></i></a>
                <ul>
                  <li><a href="#">Cabang 1</a></li>
                  <li><a href="#">Cabang 2</a></li>
                  <li><a href="#">Cabang 3</a></li>
                  <li><a href="#">Cabang 4</a></li>
                  <li><a href="#">Cabang 5</a></li>
                </ul>
              </li> -->
              <!-- <li><a href="#">Pengolahan Hasil Pertanian</a></li>
              <li><a href="#">Pusat Pelatihan Pertanian</a></li>
              <li><a href="#">Budidaya Sayuran Organik</a></li>
            </ul>
          </li>  -->
          <li><a class="nav-link scrollto" href="#contact">Kontak</a></li>
          <li><a class="nav-link scrollto" href="/Login"><strong>Login<i class="bi bi-box-arrow-in-right"></i></strong></a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->

  <!-- <section id="hero">
  <div class="hero-container">
    <h3 class="animate__animated animate__fadeInUp animate__delay-1s">Selamat Datang di <strong>Okiagaru</strong></h3>
    <h1 class="animate__animated animate__fadeInUp animate__delay-1s">Pertanian Organik</h1>
    <h2 class="animate__animated animate__fadeInUp animate__delay-2s">Pertanian organik, budidaya sayuran, pelatihan dan program magang ke Jepang</h2>
    <a href="#about" class="btn-get-started scrollto animate__animated animate__fadeInUp animate__delay-3s">Memulai</a>
  </div>
</section> -->

  <section id="hero">
    <div class="hero-container">
      <h3 class="animate__animated animate__fadeInUp animate__delay-1s">Selamat Datang di <strong>Okiagaru</strong></h3>
      <h1 class="animate__animated animate__fadeInUp animate__delay-1s">Pertanian Organik</h1>
      <h2 class="animate__animated animate__fadeInUp animate__delay-2s">Pertanian organik, budidaya sayuran, pelatihan dan program magang ke Jepang</h2>
      <a href="#about" class="btn-get-started scrollto animate__animated animate__fadeInUp animate__delay-3s">Memulai</a>
    </div>
  </section>
  
  <!-- End Hero -->

  <main id="main">

    <!-- ======= About Section ======= -->
    <section id="about" class="about">
      <div class="container">

        <div class="section-title">
          <h2 data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">Tentang</h2>
          <h3 data-aos="fade-up">Pelajari <span>Tentang Kami</span></h3>
          <p data-aos="fade-up">Berfokus kepada budidaya tanaman padi, pembuatan kompos, dan pestisida nabati. di lahan seluas 1,8 hektar sejak tahun 2004.</p>
        </div>

        <div class="row content">
          <div class="col-lg-6">
            <p data-aos="fade-right">
              {{ $isi_paragraf['isi1'] }}
            </p>

          </div>
          <div class="col-lg-6 pt-4 pt-lg-0">
            <p data-aos="fade-left">
              {{ $isi_paragraf['isi2'] }}
            </p>
            <a href="/Tentang" class="btn-learn-more" data-aos="fade-right" data-aos-offset="300" data-aos-easing="ease-in-sine">Pelajari Lebih Lanjut</a>
          </div>
        </div>

      </div>
    </section><!-- End About Section -->

    <!-- ======= Services Section ======= -->
    
    <section id="services" class="services">
      <div class="container">

        <div class="section-title">
          <h2 data-aos="fade-up">Unit</h2>
          <h3 data-aos="fade-up">Kami memiliki berbagai <span>Unit</span> perusahaan</h3>
          <p data-aos="fade-up" data-aos-duration="800">Unit perusahaan kami bergerak di bidang pertanian dan mengolah hasilnya.</p>
        </div>

        <div class="row">
          <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
          <a href="/Pertanian">
            <div class="icon-box" data-aos="fade-right" data-aos-anchor-placement="bottom-bottom" data-aos-duration="800">
              <div class="icon"><i class="fa-solid fa-leaf"></i></div>
              <h4 class="title"><a href="/Pertanian">Pertanian</a></h4>
              <p class="description">Mengelola lahan dengan hasil panen organik dan berkualitas</p>
            </div>
          </div>
         

          <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
          <a href="/Peternakan">
            <div class="icon-box" data-aos="fade-up-right" data-aos-anchor-placement="bottom-bottom" data-aos-duration="800">
              <div class="icon"><i class="fa-solid fa-cow"></i></div>
              <h4 class="title"><a href="">Peternakan</a></h4>
              <p class="description">Budidaya hewan ternak dengan kualitas tinggi dan prima</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
          <a href="/Koperasi">
            <div class="icon-box" data-aos="fade-up-left" data-aos-anchor-placement="bottom-bottom" data-aos-duration="800">
              <div class="icon"><i class="fa-solid fa-shop"></i></div>
              <h4 class="title"><a href="">Koperasi</a></h4>
              <p class="description">Menjual hasil produk olahan dengan mutu tinggi</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
          <a href="/Pelatihan">
            <div class="icon-box" data-aos="fade-left" data-aos-anchor-placement="bottom-bottom" data-aos-duration="1000">
              <div class="icon"><i class="fa-solid fa-people-roof"></i></div>
              <h4 class="title"><a href="">Pelatihan</a></h4>
              <p class="description">Pengadaan pelatihan magang ke Jepang</p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Services Section -->


    <!-- Style Buat animasi text -->
    <style>
        .ityped {
            color: white;
            font-family: 'Nunito', sans-serif;
            white-space: nowrap;
            overflow: hidden;
            border-right: 2px solid white;
        }
    </style>


    <!-- ======= Cta Section ======= -->
    <section id="cta" class="cta">
      <div class="container">

        <div class="text-center">
          <h1><span class="ityped"></span></h1>
          <div class="ityped">
            <script src="https://unpkg.com/ityped@0.0.10"></script>
            <script>
                window.ityped.init(document.querySelector('.ityped'),{
                strings: ['Petani Cerdas!','Budaya yang Positif!','Siapa Takut Jadi Petani!'],
                loop: true
              })
            </script>
            </div>
        </div>

      </div>
    </section>
    <!-- End Cta Section -->

    <!-- ======= Portfolio Section ======= -->
    <section id="portfolio" class="portfolio">
      <div class="container">

        <div class="section-title">
          <h2 data-aos="fade-up">Produk</h2>
          <h3 data-aos="fade-up">Lihat hasil <span>Produk</span> kami</h3>
          <p data-aos="fade-up">Hasil produk perusahaan dan hasil olahan di Okiagaru. Produk berkualitas prima dan organik, hasil petani cerdas.</p>
        </div>

        <div class="row">
          <div class="col-lg-12 d-flex justify-content-center">
            <ul id="portfolio-flters">
              <li data-filter="*" class="filter-active" data-aos="zoom-out-up">Semua</li>
              <li data-filter=".filter-app" data-aos="zoom-out-up">Hasil Panen</li>
              <li data-filter=".filter-card" data-aos="zoom-out-up">Olahan</li>
              <li data-filter=".filter-web" data-aos="zoom-out-up">Lainnya</li>
            </ul>
          </div>
        </div>

        <div class="row portfolio-container">

          <div class="col-lg-4 col-md-6 portfolio-item filter-app" >
            <img src="assets/img/portfolio/portfolio-1.png" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Timun</h4>
              <p>Hasil Panen</p>
              <a href="assets/img/portfolio/portfolio-1.png" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="App 1"><i class="bx bx-plus"></i></a>
              <a href="/Produk" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-web" >
            <img src="assets/img/portfolio/portfolio-2.png" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Jahe</h4>
              <p>Produk Lainnya</p>
              <a href="assets/img/portfolio/portfolio-2.png" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Web 3"><i class="bx bx-plus"></i></a>
              <a href="/Produk" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-app" >
            <img src="assets/img/portfolio/portfolio-3.png" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Bayam</h4>
              <p>Hasil Panen</p>
              <a href="assets/img/portfolio/portfolio-3.png" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="App 2"><i class="bx bx-plus"></i></a>
              <a href="/Produk" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-card" >
            <img src="assets/img/portfolio/portfolio-4.png" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Cheese Stick</h4>
              <p>Produk Olahan</p>
              <a href="assets/img/portfolio/portfolio-4.png" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Card 2"><i class="bx bx-plus"></i></a>
              <a href="/Produk" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-web" >
            <img src="assets/img/portfolio/portfolio-5.png" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Cabai Merah</h4>
              <p>Produk Lainnya</p>
              <a href="assets/img/portfolio/portfolio-5.png" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Web 2"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-app" >
            <img src="assets/img/portfolio/portfolio-6.png" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Bawang Merah</h4>
              <p>Hasil Panen</p>
              <a href="assets/img/portfolio/portfolio-6.png" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="App 3"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-card" >
            <img src="assets/img/portfolio/portfolio-7.png" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Daging Sapi</h4>
              <p>Produk Olahan</p>
              <a href="assets/img/portfolio/portfolio-7.png" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Card 1"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-card" >
            <img src="assets/img/portfolio/portfolio-8.png" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Susu</h4>
              <p>Produk Olahan</p>
              <a href="assets/img/portfolio/portfolio-8.png" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Card 3"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-web" >
            <img src="assets/img/portfolio/portfolio-9.png" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Labu</h4>
              <p>Produk Lainnya</p>
              <a href="assets/img/portfolio/portfolio-9.png" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Web 3"><i class="bx bx-plus"></i></a>
              <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Portfolio Section -->



    <!-- ======= Team Section ======= -->
    <section id="team" class="team">
      <div class="container">

        <div class="section-title">
          <h2 data-aos="fade-up">Tim</h2>
          <h3 data-aos="fade-up"><span>Tim</span> kami</h3>
          <p data-aos="fade-up">Ketua, pendiri, dan karyawan terbaik kami</p>
        </div>

        <div class="row">

          <div class="col-lg-3 col-md-6 d-flex align-items-stretch" data-aos="fade-right">
            <div class="member">
              <div class="member-img">
                <img src="assets/img/team/team-1.jpg" class="img-fluid" alt="">
                <div class="social">
                  <a href=""><i class="bi bi-twitter"></i></a>
                  <a href=""><i class="bi bi-facebook"></i></a>
                  <a href=""><i class="bi bi-instagram"></i></a>
                  <a href=""><i class="bi bi-linkedin"></i></a>
                </div>
              </div>
              <div class="member-info">
                <h4>Agus Ali Nurdin, SE</h4>
                <span>Chairman</span>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 d-flex align-items-stretch" data-aos="fade-right">
            <div class="member">
              <div class="member-img">
                <img src="assets/img/team/team-2.jpg" class="img-fluid" alt="">
                <div class="social">
                  <a href=""><i class="bi bi-twitter"></i></a>
                  <a href=""><i class="bi bi-facebook"></i></a>
                  <a href=""><i class="bi bi-instagram"></i></a>
                  <a href=""><i class="bi bi-linkedin"></i></a>
                </div>
              </div>
              <div class="member-info">
                <h4>Mia Nurhasanah</h4>
                <span>Financial</span>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 d-flex align-items-stretch" data-aos="fade-left">
            <div class="member">
              <div class="member-img">
                <img src="assets/img/team/team-3.jpg" class="img-fluid" alt="">
                <div class="social">
                  <a href=""><i class="bi bi-twitter"></i></a>
                  <a href=""><i class="bi bi-facebook"></i></a>
                  <a href=""><i class="bi bi-instagram"></i></a>
                  <a href=""><i class="bi bi-linkedin"></i></a>
                </div>
              </div>
              <div class="member-info">
                <h4>Yuki Aramdhani</h4>
                <span>Secretary</span>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 d-flex align-items-stretch" data-aos="fade-left">
            <div class="member">
              <div class="member-img">
                <img src="assets/img/team/team-4.jpg" class="img-fluid" alt="">
                <div class="social">
                  <a href=""><i class="bi bi-twitter"></i></a>
                  <a href=""><i class="bi bi-facebook"></i></a>
                  <a href=""><i class="bi bi-instagram"></i></a>
                  <a href=""><i class="bi bi-linkedin"></i></a>
                </div>
              </div>
              <div class="member-info">
                <h4>Abdul Hamid</h4>
                <span>Human Resource</span>
              </div>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Team Section -->

    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
      <div class="container">

        <div class="section-title">
          <h2 data-aos="fade-up">Kontak</h2>
          <h3 data-aos="fade-up"><span>Hubungi</span> kami</h3>
          <p data-aos="fade-up">Silahkan hubungi kami dengan memberikan masukan dan pesan.</p>
        </div>

        <div data-aos="fade-up">
          <iframe style="border:0; width: 100%; height: 270px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3962.024227975717!2d107.05995637560176!3d-6.766899993229832!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69b3f3c95bda13%3A0x5ae76979623f5b6!2sOkiagaru%20Farm!5e0!3m2!1sid!2sid!4v1692432737568!5m2!1sid!2sid" frameborder="0" allowfullscreen></iframe>
        </div>

        <div class="row mt-5">

          <div class="col-lg-4">
            <div class="info">
              <div class="address">
                <i class="bi bi-geo-alt"></i>
                <h4>Lokasi :</h4>
                <p>Kp Tunggilis, Ciputri, Kec. Pacet, Kabupaten Cianjur, Jawa Barat 43253</p>
              </div>
            
              <div class="email">
                <i class="bi bi-envelope"></i>
                <h4>Email :</h4>
                <p>info@example.com</p>
              </div>

              <div class="phone">
                <i class="bi bi-phone"></i>
                <h4>Telepon :</h4>
                <p>+62 856 9128 1247</p>
              </div>

            </div>

          </div>

          <div class="col-lg-8 mt-5 mt-lg-0">

            <form action="forms/contact.php" method="post" role="form" class="php-email-form">
              <div class="row">
                <div class="col-md-6 form-group">
                  <input type="text" name="name" class="form-control" id="name" placeholder="Nama" required>
                </div>
                <div class="col-md-6 form-group mt-3 mt-md-0">
                  <input type="email" class="form-control" name="email" id="email" placeholder="Email" required>
                </div>
              </div>
              <div class="form-group mt-3">
                <input type="text" class="form-control" name="subject" id="subject" placeholder="Subjek" required>
              </div>
              <div class="form-group mt-3">
                <textarea class="form-control" name="message" rows="5" placeholder="Pesan" required></textarea>
              </div>
              <div class="my-3">
                <div class="loading">Loading</div>
                <div class="error-message"></div>
                <div class="sent-message">Your message has been sent. Thank you!</div>
              </div>
              <div class="text-center"><button type="submit">Kirim Pesan <br><i class="fa-solid fa-envelope"></i></button></div>
            </form>

          </div>

        </div>

      </div>
    </section><!-- End Contact Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">

    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-contact">
            <h3>Okiagaru</h3>
            <p>
              Kp Tunggilis, Ciputri, Kec. Pacet, <br>
              Kabupaten Cianjur, Jawa Barat 43253<br>
              Indonesia <br><br>
              <strong>Phone:</strong> +62 856 9128 1247<br>
              <strong>Email:</strong> contoh@email.com<br>
            </p>
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Menu</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Beranda</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Tentang</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Unit</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Produk</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Tim</a></li>
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Unit kami</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Pertanian</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Peternakan</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Koperasi</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Pelatihan</a></li>
            </ul>
          </div>

          <div class="col-lg-4 col-md-6 footer-newsletter">
            <h4>Mari bergabung</h4>
            <p>isi email untuk berlangganan</p>
            <form action="" method="post">
              <input type="email" name="email" placeholder="contoh@email.com"><input type="submit" value="Subscribe">
            </form>
          </div>

        </div>
      </div>
    </div>

    <div class="container d-md-flex py-4">

      <div class="me-md-auto text-center text-md-start">
        <div class="copyright">
          &copy; Copyright <strong><span>Okiagaru</span></strong>. 2023
        </div>
        <div class="credits">
          <!-- All the links in the footer should remain intact. -->
          <!-- You can delete the links only if you purchased the pro version. -->
          <!-- Licensing information: https://bootstrapmade.com/license/ -->
          <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/tempo-free-onepage-bootstrap-theme/ -->

        </div>
      </div>
      <div class="social-links text-center text-md-right pt-3 pt-md-0">
        <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
        <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
        <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
        <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
        <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
      </div>
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>

