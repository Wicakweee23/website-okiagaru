<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Okiagaru | Login</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="assets/img/favicon.png" rel="icon">
    <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
    <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">
    <link href="assets/fontawesome/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/preloader/style.css">
    <script src="assets/preloader/script.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
    
    <!-- Template Main CSS File -->
    <link href="assets/css/style.css" rel="stylesheet">

    <!-- =======================================================
      * Template Name: Tempo
      * Updated: Jul 27 2023 with Bootstrap v5.3.1
      * Template URL: https://bootstrapmade.com/tempo-free-onepage-bootstrap-theme/
      * Author: BootstrapMade.com
      * License: https://bootstrapmade.com/license/
      ======================================================== -->
</head>

<body>
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script>
        AOS.init();
    </script>
    <div id="preloader">
        <div id="loader"></div>
    </div>

    <!-- Form Login Disini Mulai -->
    <div style="display: flex; justify-content: center; align-items: center; min-height: 100vh; position: relative;">
        <div class="background-layer"></div>
        <div class="card" style="border: 2px solid #ccc; border-radius: 10px; background-color: rgba(255, 255, 255, 0.8); width: 80%; max-width: 400px; box-shadow: 0px 0px 100px rgba(0, 0, 0, 11.5);">
            <div class="card-header" style="background-color: #e43c5c; color: #fff; border-top-left-radius: 10px; border-top-right-radius: 10px; padding: 8px;">
                <h2><strong>Login</strong></h2>
            </div>

            @if(session()->has('loginError'))
                <div class="alert alert-danger alert-dismissible fade show" rolee="alert">
                    {{ session('loginError') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif


            <div class="card-body" style="padding: 20px;">
                <form action="/Login" method="post" style="text-align: center;">
                @csrf 
                    <div class="form-group" style="margin-bottom: 15px;">
                        <label for="email" style="font-weight: bold; font-size: 18px; color: #333;">Email</label>
                        <input class="@error('email') is-invalid @enderror" value="{{ old('email') }}" autofocus type="text" id="email" name="email" placeholder="contoh@email.com" required style="padding: 10px; width: 100%; border: 1px solid #ccc; border-radius: 5px;">
                            @error('email')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                            @enderror
                    </div>
                    <div class="form-group" style="margin-bottom: 20px;">
                        <label for="password" style="font-weight: bold; font-size: 18px; color: #333;">Password</label>
                        <input type="password" id="password" name="password" placeholder="*****" required style="padding: 10px; width: 100%; border: 1px solid #ccc; border-radius: 5px;">
                    </div>
                    <div class="form-group">
                        <button type="submit" style="background-color: #e43c5c; color: #fff; border: none; border-radius: 5px; padding: 10px 20px; font-weight: bold; font-size: 18px;">Login</button>
                    </div>
                </form>
                <div style="text-align: center;">
                    <p>Tidak memiliki akun? <a href="/Register" style="color: #e43c5c; text-decoration: none;">Register disini</a>.</p>
                </div>
            </div>
        </div>
    </div>
    <!-- Form Login Cardnya Beres -->

    <style>
        .background-layer {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-image: url('assets/img/tentang.jpg'); /* Ganti dengan URL gambar Anda */
            background-size: cover;
            filter: brightness(65%); /* Sesuaikan nilai brightness sesuai kebutuhan */
            z-index: -1;
        }
    </style>
</body>

</html>
