<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Okiagaru | Tentang</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">
  <link href="assets/fontawesome/css/all.css" rel="stylesheet">
  <link rel="stylesheet" href="assets/preloader/style.css">
  <script src="assets/preloader/script.js"></script>
  <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>


  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">
  <link href="assets/css/timeline.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Tempo
  * Updated: Jul 27 2023 with Bootstrap v5.3.1
  * Template URL: https://bootstrapmade.com/tempo-free-onepage-bootstrap-theme/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>
<script src="https://unpkg.com/aos@next/dist/aos.js"></script>

<script>
    AOS.init();
  </script>

<div id="preloader">
    <div id="loader"></div>
  </div>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top header-inner-pages">
    <div class="container d-flex align-items-center justify-content-between">

      <!-- <h1 class="logo"><a href="/">Okiagaru</a></h1> -->
      <!-- Uncomment below if you prefer to use an image logo -->
      <a href="/" class="logo"><img src="assets/img/logo.png" alt="logo okiagaru" class="img-fluid"></a>
      <!-- <a href="index.html" class="logo"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

      <nav id="navbar" class="navbar">
        <ul>
          <li><a class="nav-link scrollto" href="/">Beranda</a></li>
          <li><a class="nav-link scrollto active" href="/">Tentang</a></li>
          <li><a class="nav-link scrollto" href="/">Unit</a></li>
          <li><a class="nav-link scrollto " href="/">Produk</a></li>
          <li><a class="nav-link scrollto" href="/">Tim</a></li>
          <!-- Navbar Cabang -->
          <li class="dropdown"><a href="#"><span>Program</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
              <li><a href="#">Magang ke Jepang</a></li>
              <!-- Buka Comment untuk menampilkan cabang cabang navigasi -->
              <!-- <li class="dropdown"><a href="#"><span>Program Cabang</span> <i class="bi bi-chevron-right"></i></a>
                <ul>
                  <li><a href="#">Cabang 1</a></li>
                  <li><a href="#">Cabang 2</a></li>
                  <li><a href="#">Cabang 3</a></li>
                  <li><a href="#">Cabang 4</a></li>
                  <li><a href="#">Cabang 5</a></li>
                </ul>
              </li> -->
              <li><a href="#">Pengolahan Hasil Pertanian</a></li>
              <li><a href="#">Pusat Pelatihan Pertanian</a></li>
              <li><a href="#">Budidaya Sayuran Organik</a></li>
            </ul>
          </li>
          <li><a class="nav-link scrollto" href="#contact">Kontak</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->


  <main id="main">

<!-- ======= Breadcrumbs ======= -->
<section id="breadcrumbs" class="breadcrumbs">
  <div class="container">

    <ol>
      <li><a href="/">Beranda</a></li>
      <li>Tentang</li>
    </ol>
    <h2>Tentang Kami</h2>

  </div>
</section><!-- End Breadcrumbs -->

<!-- ======= About Section ======= -->
<section class="about">
      <div class="container">

        <div class="section-title">
          <h2 data-aos="fade-up">Tentang</h2>
          <h3 data-aos="fade-up"><span>Video</span> Profil Okiagaru</h3>
          <br><br>
          <!-- buka comment dibawah ini jika ingin menambahkan sub judul berupa 1 - 3 kalimat -->
          <div class="video-container">
                                <video controls style="max-width: 100%; height: auto;">
                                    <source src="assets/video/Okiagaru - Drone Footage.mp4" type="video/mp4">
                                    Your browser does not support the video tag.
                                </video>
                            </div>
        </div>

     
        <p>Pengambilan gambar oleh drone tersebut dapat menggambarkan luasnya lahan yang dimiliki Okiagaru. Lahan tersebut dominan dihuni oleh para sayuran berkualitas.</p>
        <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Saepe culpa ea soluta asperiores, unde ratione, nesciunt placeat magni quod fugit alias, eum veniam adipisci numquam inventore. Consequuntur est illum maxime.</p>
        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Temporibus commodi quae sunt ab, doloribus nam, harum ipsa deserunt voluptas neque labore, blanditiis nisi! Est vitae fuga commodi quidem sit illum.</p>

          </div>
        </div>

      </div>
    </section><!-- End About Section -->

    <!-- Frame Buat Video --



<!-- ======= Portfolio Details Section ======= -->
<!-- <section id="portfolio-details" class="portfolio-details">

      <div class="container">

        <div class="section-title">

     
        
          

</section> -->

<!-- End Portfolio Details Section -->

  <!-- ======= Hero Section ======= -->


  <main id="main">

    <!-- ======= About Section ======= -->
<section class="about">
  
    <div class="section-title">
      <h3 data-aos="fade-up"><span>Sejarah</span> Okiagaru</h3>
    </div>


    
<ul class="timeline">

	<!-- Item 1 -->
	<li>
		<div class="direction-r">
			<div class="flag-wrapper">
				<span class="flag">Pendirian Okiagaru</span>
				<span class="time-wrapper"><span class="time">2004</span></span>
			</div>
			<div class="desc">Didirikan pada 25 Desember 2004 di Desa Siliwangi oleh Agus Ali Nurdin, SE.</div>
		</div>
	</li>
  
	<!-- Item 2 -->
	<li>
		<div class="direction-l">
			<div class="flag-wrapper">
				<span class="flag">Fokus pada Pertanian Awal</span>
				<span class="time-wrapper"><span class="time">2004 - 2008</span></span>
			</div>
			<div class="desc">Mr. Guslee mengikuti program magang petani muda ASEAN di Jepang untuk meningkatkan SDM petani muda</div>
		</div>
	</li>

	<!-- Item 3 -->
	<li>
		<div class="direction-r">
			<div class="flag-wrapper">
				<span class="flag">Transformasi Okiagaru Farm</span>
				<span class="time-wrapper"><span class="time">Maret 2009</span></span>
			</div>
			<div class="desc">Mengubah Okiagaru menjadi Okiagaru Farm dengan spesialisasi dalam budidaya sayuran Jepang.</div>
		</div>
	</li>

  <!-- Item 4 -->
	<li>
		<div class="direction-l">
			<div class="flag-wrapper">
				<span class="flag">Lokasi Baru di Cianjur</span>
				<span class="time-wrapper"><span class="time">2009</span></span>
			</div>
			<div class="desc">Memilih lokasi baru di Kabupaten Cianjur, Jawa Barat, dengan lahan seluas 3,5 hektar.</div>
		</div>
	</li>

	<!-- Item 5 -->
	<li>
		<div class="direction-r">
			<div class="flag-wrapper">
				<span class="flag">Pengembangan dan Kerja Sama</span>
				<span class="time-wrapper"><span class="time">2010</span></span>
			</div>
			<div class="desc">Menjalin kerja sama dengan berbagai lembaga bisnis.</div>
		</div>
	</li>

  <!-- Item 6 -->
	<li>
		<div class="direction-l">
			<div class="flag-wrapper">
				<span class="flag">Kelompok Pemuda Tani</span>
				<span class="time-wrapper"><span class="time">Juli 2014</span></span>
			</div>
			<div class="desc">Resmi dikukuhkan oleh Kepala Dinas Pertanian Tanaman Pangan dan Hortikultura sebagai Kelompok Pemuda Tani.</div>
		</div>
	</li>

	<!-- Item 7 -->
	<li>
		<div class="direction-r">
			<div class="flag-wrapper">
				<span class="flag">Lokasi Baru</span>
				<span class="time-wrapper"><span class="time">2014 - Sekarang</span></span>
			</div>
			<div class="desc">Berpindah lokasi ke Kampung Tunggilis Desa Ciputri, setelah kontrak lahan sebelumnya berakhir.</div>
		</div>
	</li>

  <!-- Item 8 -->
	<li>
		<div class="direction-l">
			<div class="flag-wrapper">
				<span class="flag">Pengembangan Berkelanjutan</span>
				<span class="time-wrapper"><span class="time">2014 - Sekarang</span></span>
			</div>
			<div class="desc">Okiagaru terus menunjukkan komitmen dalam mengembangkan pertanian berkelanjutan, kewiraushaan, dan pembangunan komunitas di bidang pertanian dan agribisnis.</div>
		</div>
	</li>
  
</ul>
    

</section>
<!-- End About Section -->




    <!-- Frame Buat Video -->
    <!-- ======= Contact Section ======= -->
    <section id="" class="contact">
      <div class="container">

        <div class="section-title">
        </div>




      
<!-- ======= Portfolio Details Section ======= -->
<!-- <section id="portfolio-details" class="portfolio-details">
      <div class="container">

        <div class="row gy-4">

          <div class="col-lg-8">
            <div class="portfolio-details-slider swiper">
              <div class="swiper-wrapper align-items-center">

                <div class="">
                  <iframe width="854" height="480" src="assets/video/Okiagaru - Drone Footage.mp4" allow="autoplay" frameborder="0" allowfullscreen></iframe>
                </div>


              </div>
              <div class="swiper-pagination"></div>
            </div>
          </div>

          <div class="col-lg-4">
            <div class="portfolio-description">
              <h2>Deskripsi</h2>
              <p>
                Pengambilan gambar oleh drone tersebut dapat menggambarkan luasnya lahan yang dimiliki Okiagaru. Lahan tersebut dominan dihuni oleh para sayuran berkualitas.
              </p>
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Iure asperiores, minima, fugiat voluptatibus illum rem assumenda corporis quia, id cupiditate sed ut rerum unde modi quas accusantium sequi quod? Laudantium?
              </p>
              <p>
                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Rerum, non debitis. Assumenda optio, suscipit unde cumque minima molestiae corrupti iure illo perspiciatis ad incidunt vero, quia repudiandae ex eos quibusdam.
              </p>
            </div>
          </div>

        </div>

      </div>
    </section> -->
    <!-- End Portfolio Details Section -->


        <!-- Buat Nampilin Video -->
<!--         
        <div data-aos="fade-down">
          <iframe width="854" height="480" src="assets/video/Okiagaru - Drone Footage.mp4" allow="autoplay" frameborder="0" allowfullscreen></iframe>
        </div> -->
        
<!-- <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br> -->



        
    <!-- ======= About Section ======= -->
<section class="about">
  <div class="container">

    <div class="section-title">
      
      <h3 data-aos="fade-up"><span>Visi</span> Misi</h3>
      <!-- buka comment dibawah ini jika ingin menambahkan sub judul berupa 1 - 3 kalimat -->
      <!-- <p>Berfokus kepada budidaya tanaman padi, pembuatan kompos, dan pestisida nabati. di lahan seluas 1,8 hektar sejak tahun 2004.</p> -->
    </div>

    <div class="row content">
      <div class="col-lg-6">
        <h3 data-aos="fade-up" style="text-align: center;"><span>Visi</span></h3>
        <p data-aos="zoom-out-right">
        Menjadi lembaga agribisnis petani muda yang mandiri, inovatif, profesional, bertaraf internasional, berbasis ekonomi syariah, dan ramah lingkungan.
        </p>

      </div>
      <div class="col-lg-6 pt-4 pt-lg-0">
        <h3 data-aos="fade-up" style="text-align: center;"><span></span> Misi</h3>
        <p data-aos="zoom-out-left">
        1. Meningkatkan daya saing petani sebagai pelaku utama dalam sistem agribisnis. <br>
        2. Menghasilkan produk pertanian yang sehat dan kontinyu dengan kualitas tinggi.<br>
        3. Menciptakan teknologi tepat guna dan nilai tambah produk pertanian.<br>
        4. Menerapkan sistem kerja yang mengacu pada SKKNI / SNI.<br>
        5. Menerapkan sistem agribisnis berdasarkan kaidah - kaidah syariah.
        </p>

      </div>
    </div>

  </div>
</section><!-- End About Section -->

    <section class="about">
      <div class="container">

        <div class="section-title">
          <h3 data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">Kondisi <span>dan</span> Manajemen Okiagaru</h3>
          <!-- buka comment dibawah ini jika ingin menambahkan sub judul berupa 1 - 3 kalimat -->
          <!-- <p>Berfokus kepada budidaya tanaman padi, pembuatan kompos, dan pestisida nabati. di lahan seluas 1,8 hektar sejak tahun 2004.</p> -->
        </div>

        <div class="row content">
          <div class="col-lg-6">
            <p data-aos="zoom-out-right">
            <br><br><br>Okiagaru Farm berlokasi di kampung Tunggilis, Desa ciputri,
Kecamatan Pacet, Kabupaten Cianjur, Jawa Barat yang secara geografis terletak di kaki Gunung Gede yang sebagian besar merupakan daerah dataran tinggi pegunungan dan sebagian lagi merupakan areal perkebunan dan persawahan. Kabupaten Cianjur berada pada ketinggian 1.110 mdpl dan berada pada perbukitan berelief halus dengan kemiringan lereng 8-15%. Curah hujan rata-rata berkisar 1.000-1.500 mm/tahun. Jenis tanah pada wilayah Kabupaten Cianjur didominasi oleh tiga jenis tanah yaitu regosol,andosol,dan latosol, sedangkan di wilayah Desa ciputri memiliki jenis tanah latosol. Kabupaten Cianjur memang terkenal dengan kawasan pertaniannya dan komoditas unggulan pada produk hortikultura. <br> <br>
Visa merupakan rangkaian kalimat yang menyatakan cita-cita atau impian sebuah organisasi atau perusahaan yang ingin dicapai di masa depan. Visi juga merupakan hal yang sangat penting bagi perusahaan untuk menjamin kelestarian dan kesuksesan jangka panjang. Dalam mencapai sebuah visi, diperlukan sebuah program khusus yang mampu dilakukan oleh perusahaan yang selanjutnya akan menjadi sebuah pernyataan usaha. Visi dan misi dibuat dengan menyesuaikan usaha dan bidang yang digeluti oleh perusahaan. Okiagaru Farm dalam menjalankan usahanya yang bergerak dibidang agribisnis, khususnya sayuran jepang.
            </p>

          </div>
          <div class="col-lg-6 pt-4 pt-lg-0">
         <br><br><br>
              <img src="assets/img/tentang.jpg" alt="Lahan Okiagaru" width="640" data-aos="flip-left" data-aos-easing="ease-out-cubic" data-aos-duration="2000">
       

          </div>
        </div>

      </div>
    </section><!-- End About Section -->

        <!-- ======= Portfolio Section ======= -->
        <section id="" class="portfolio">
      <div class="container">

        <div class="section-title">

        </div>

        <div class="row">
          <div class="col-lg-12 d-flex justify-content-center">
            <ul id="portfolio-flters">

            </ul>
          </div>
        </div>


        <div class="section-title">
      
      <h3 data-aos="fade-up">Berbagai <span>Kegiatan</span> Kami</h3>
      <!-- buka comment dibawah ini jika ingin menambahkan sub judul berupa 1 - 3 kalimat -->
      <!-- <p>Berfokus kepada budidaya tanaman padi, pembuatan kompos, dan pestisida nabati. di lahan seluas 1,8 hektar sejak tahun 2004.</p> -->
    </div>


        <div class="row portfolio-container">

          <div class="col-lg-4 col-md-6 portfolio-item filter-app" data-aos="zoom-in-up">
            <img src="assets/img/portfolio/logo.png" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Logo</h4>
              <p>Okiagaru</p>
              <a href="assets/img/portfolio/logo.png" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Logo Okiagaru"><i class="bx bx-plus"></i></a>
              
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-web" data-aos="zoom-in-up" data-aos-duration="600">
            <img src="assets/img/portfolio/11.png" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Pos Utama</h4>
              <p>Okiagaru</p>
              <a href="assets/img/portfolio/11.png" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Pos Utama"><i class="bx bx-plus"></i></a>
              
            </div>
          </div>

          <div class="col-lg-4 col-md-6 portfolio-item filter-app" data-aos="zoom-in-up" data-aos-duration="800">
            <img src="assets/img/portfolio/12.png" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4>Sesi Berbagi</h4>
              <p>Okiagaru</p>
              <a href="assets/img/portfolio/12.png" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="Sesi Berbagi"><i class="bx bx-plus"></i></a>
            </div>
          </div>






  </main><!-- End #main -->
  <main id="main">


  
<br>

  <!-- ======= Footer ======= -->
  <footer id="footer">

    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-contact">
            <h3>Okiagaru</h3>
            <p>
              Kp Tunggilis, Ciputri, Kec. Pacet, <br>
              Kabupaten Cianjur, Jawa Barat 43253<br>
              Indonesia <br><br>
              <strong>Phone:</strong> +62 856 9128 1247<br>
              <strong>Email:</strong> contoh@email.com<br>
            </p>
          </div>

          <div class="col-lg-2 col-md-6 footer-links">
            <h4>Menu</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Beranda</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Tentang</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Unit</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Produk</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Tim</a></li>
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Unit kami</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Pertanian</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Peternakan</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Koperasi</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Pelatihan</a></li>
            </ul>
          </div>

          <div class="col-lg-4 col-md-6 footer-newsletter">
            <h4>Mari bergabung</h4>
            <p>isi email untuk berlangganan</p>
            <form action="" method="post">
              <input type="email" name="email" placeholder="contoh@email.com"><input type="submit" value="Subscribe">
            </form>
          </div>

        </div>
      </div>
    </div>

    <div class="container d-md-flex py-4">

      <div class="me-md-auto text-center text-md-start">
        <div class="copyright">
          &copy; Copyright <strong><span>Okiagaru</span></strong>. 2023
        </div>
        <div class="credits">
          <!-- All the links in the footer should remain intact. -->
          <!-- You can delete the links only if you purchased the pro version. -->
          <!-- Licensing information: https://bootstrapmade.com/license/ -->
          <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/tempo-free-onepage-bootstrap-theme/ -->

        </div>
      </div>
      <div class="social-links text-center text-md-right pt-3 pt-md-0">
        <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
        <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
        <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
        <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
        <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
      </div>
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>